package model;

public class Kontakt {
	private long id; //kvuli databazi, primarni klic
	
	private String jmeno;
	private String prijmeni;
	private String telefon;
	private Adresa adresa;
	
	public Kontakt (){
		id = 0;
	}


	public long getId() {
		return id;
	}

	
	public void setId(long id) {
		this.id = id;
	}

	
	public String getJmeno() {
		return jmeno;
	}

	
	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}

	
	public String getPrijmeni() {
		return prijmeni;
	}

	
	public void setPrijmeni(String prijmeni) {
		this.prijmeni = prijmeni;
	}

	
	public String getTelefon() {
		return telefon;
	}

	
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	
	public Adresa getAdresa() {
		return adresa;
	}

	
	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}
	
}

package model;

public class Adresa {
	private String ulice;
	private String cp;
	private String obec;
	private String psc;
	
	public String getUlice() {
		return ulice;
	}

	public void setUlice(String ulice) {
		this.ulice = ulice;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getObec() {
		return obec;
	}

	public void setObec(String obec) {
		this.obec = obec;
	}

	public String getPsc() {
		return psc;
	}

	public void setPsc(String psc) {
		this.psc = psc;
	}
	
	
	
}

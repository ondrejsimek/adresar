package gui;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import logika.AdresarServices;
import model.Kontakt;

public class AdresarTableModel extends AbstractTableModel {
	private AdresarServices as = null;
	private List<Kontakt> cache;	//kvuli pozdejsi praci s DB
	
	private static final String[] sloupce = {
		"P��jmen�","Jm�no","Telefon","Ulice","�P","Obec","PS�"
	};
	
	public AdresarTableModel(AdresarServices as){
		this.as=as;
	}
	
	public void refresh(){
		cache = as.getAll();
		fireTableDataChanged();	//znovuna�ten� dat z datab�ze, po p�id�n� ��dku do tabulky se tabulka aktualizuje, kdyby zde nebyl tento prikaz, tak by se sice data dala do databaze, ale nevideli bysme je ihned v tabulce
	}
	
	@Override
	public int getRowCount() {
		return cache.size();	//podle poctu zaznamu v databazi
	}

	@Override
	public int getColumnCount() {
		return sloupce.length;
	}
	
	public String getColumnName(int col){
		return sloupce[col];
	}

	@Override
	public Object getValueAt(int rowIndex, int colIndex) {
		Kontakt knt = cache.get(rowIndex);
		switch (colIndex){
		case 0:
			return knt.getPrijmeni();
		case 1:
			return knt.getJmeno();
		case 2:
			return knt.getTelefon();
		case 3:
			return knt.getAdresa().getUlice();
		case 4:
			return knt.getAdresa().getCp();
		case 5:
			return knt.getAdresa().getObec();
		case 6:
			return knt.getAdresa().getPsc();
		
		default:
			return "";
		}
	}

	
	/**
	 * Vraci referenci na kontakt na zadanem radku model
	 * @return
	 */
	public Kontakt getKontaktByRowIndex(int row) {
		return cache.get(row);
	}
	
	public boolean isCellEditable(int row, int col) {
		return false;
	}
}

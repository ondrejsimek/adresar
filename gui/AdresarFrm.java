package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import logika.AdresarServices;
import logika.InMemoryAdresarServices;
import model.Kontakt;

public class AdresarFrm extends JFrame {
	private AdresarServices as = new InMemoryAdresarServices();
	private KontaktDialog kntDialog = new KontaktDialog(this);
	
	
	private AdresarTableModel tblModel;
	private JTable tab;
	private Action actAdd,actRemove,actRefresh,actEdit,actExit;	//Akce, kter· se provede po kliknutÌ na tlaËÌtko
	
	public AdresarFrm(){
		super("Adresář");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initGUI();
		setSize(640, 480);
	}
	
	private void initGUI(){
		vytvroAkce();
		getJMenuBar();
		vytvorToolbar();
		vytvorTabulku();
	}

	private void vytvorTabulku() {
		tblModel = new AdresarTableModel(as);
		tblModel.refresh();
		
		tab = new JTable(tblModel);
		
		tab.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tab.setPreferredScrollableViewportSize(new Dimension(200,100));
		tab.setToolTipText("Seznam osob v evidenci");	//bublinova napoveda
		
		//Pokud bude vÌce ¯·dk˘, tak v textovÈ komponentÏ neexistuje posuvnÌk, musÌme ho doplnit pomocÌ tohoto.
		JScrollPane sp = new JScrollPane(tab);
		sp.setBorder(BorderFactory.createTitledBorder("Seznam osob."));
		
		tab.setAutoCreateRowSorter(true);
		
		add(sp,"Center");
	}

	private void vytvorToolbar() {
		JToolBar tb = new JToolBar("Nástroje", JToolBar.HORIZONTAL);
		tb.add(actAdd);
		tb.add(actRemove);
		tb.add(actEdit);
		tb.addSeparator();
		tb.add(actRefresh);
		tb.addSeparator();
		tb.add(actExit);
		tb.setRollover(false);
		add(tb, "North");
		
	}

	private void vytvroAkce() {
		actAdd = new AbstractAction("Přidat") {
			public void actionPerformed(ActionEvent e) {
				kntDialog.vymazTextfieldy();
				kntDialog.spustAdd();
				
				if (kntDialog.IsOk()); {
					Kontakt knt = kntDialog.getKontakt();
					as.store(knt);
					tblModel.refresh();
				}
			}
		};
		
		actRemove = new AbstractAction("Odstranit") {
			public void actionPerformed(ActionEvent arg0) {
				int ind = tab.getSelectedRow();
				
				if (ind != -1) {
					ind = tab.convertRowIndexToModel(ind);
					Kontakt knt = tblModel.getKontaktByRowIndex(ind);
					as.remove(knt.getId());
					tblModel.refresh();
				}
			}
		};
		
		actEdit = new AbstractAction("Upravit") {
			public void actionPerformed(ActionEvent e) {
				int ind = tab.getSelectedRow();
				if (ind != -1) {
					ind = tab.convertRowIndexToModel(ind);
					Kontakt knt = tblModel.getKontaktByRowIndex(ind);
					if (kntDialog.spustEdit(knt)) {
						as.store(kntDialog.getKontakt());
						tblModel.refresh();
					}
				}
			}
		};
		
		actRefresh = new AbstractAction("Obnovit") {
			public void actionPerformed(ActionEvent e) {
				tblModel.refresh();
			}
		};
		
		actExit = new AbstractAction("Konec") {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		};
	}
	
	
	private void vytvorMenubar() {
		JMenuBar mb = new JMenuBar();
		JMenu mnHlavni = new JMenu("Nabídka");
		mnHlavni.add(actAdd);
		mnHlavni.add(actRemove);
		mnHlavni.add(actEdit);
		mnHlavni.addSeparator();
		mnHlavni.add(actRefresh);
		mnHlavni.addSeparator();
		mnHlavni.add(actExit);
		
		mb.add(mnHlavni);
		setJMenuBar(mb);
		

	}
}

package gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Adresa;
import model.Kontakt;

public class KontaktDialog extends JDialog {
	
	private Kontakt kontakt = null;
	private boolean ok = false;
	
	private Font fnt = new Font("Courier New", Font.BOLD, 15);
	
	private JTextField tfJmeno = new JTextField(10);
	private JTextField tfPrijmeni = new JTextField(10);
	private JTextField tfTelefon = new JTextField(10);
	private JTextField tfUlice = new JTextField(10);
	private JTextField tfCp = new JTextField(4);
	private JTextField tfObec = new JTextField(10);
	private JTextField tfPsc= new JTextField(6);
	
	private JButton btOk = new JButton("Ok");
	private JButton btStorno = new JButton("Storno");
	
	public KontaktDialog(JFrame vlastnik) {
		super(vlastnik, "Kontakt  - detail", true); //vlastnik, nazev, je/neni modalni; true = modalni, nelze psat do hlavniho okna je-li v provozu
		initGUI();
		pack();
		setSize(getWidth() + 30, getHeight() + 30);
		setLocationRelativeTo(getOwner()); //nastavi umerne k hlavnimu oknu
		setResizable(false);
	}
	
	private void initGUI() {
		
		tfJmeno.setBackground(Color.WHITE);
		tfJmeno.setFont(fnt);
		JLabel lbJmeno = new JLabel("Jmeno:");
		
		tfPrijmeni.setBackground(Color.WHITE);
		tfPrijmeni.setFont(fnt);
		JLabel lbPrijmeni = new JLabel("Prijmeni:");
		
		tfTelefon.setBackground(Color.WHITE);
		tfTelefon.setFont(fnt);
		JLabel lbTelefon = new JLabel("Telefon:");
		
		tfUlice.setBackground(Color.WHITE);
		tfUlice.setFont(fnt);
		JLabel lbUlice = new JLabel("Ulice:");
		
		tfCp.setBackground(Color.WHITE);
		tfCp.setFont(fnt);
		JLabel lbCp = new JLabel("cp:");
		
		tfObec.setBackground(Color.WHITE);
		tfObec.setFont(fnt);
		JLabel lbObec = new JLabel("Obec:");
		
		tfPsc.setBackground(Color.WHITE);
		tfPsc.setFont(fnt);
		JLabel lbPsc = new JLabel("psc:");
		
		ActionListener actListener = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == btOk){
					vyplnKontakt();
					ok = true;
				} else {
					ok = false;
				}
				setVisible(false);
				
			}
		};
		
		JPanel pnlPolozky = new JPanel(new GridLayout(7, 2));
		pnlPolozky.add(lbJmeno);
		pnlPolozky.add(tfJmeno);
		pnlPolozky.add(lbPrijmeni);
		pnlPolozky.add(tfPrijmeni);
		pnlPolozky.add(lbTelefon);
		pnlPolozky.add(tfTelefon);
		pnlPolozky.add(lbUlice);
		pnlPolozky.add(tfUlice);
		pnlPolozky.add(lbCp);
		pnlPolozky.add(tfCp);
		pnlPolozky.add(lbObec);
		pnlPolozky.add(tfObec);
		pnlPolozky.add(lbPsc);
		pnlPolozky.add(tfPsc);
		
		
		btOk.addActionListener(actListener);
		btStorno.addActionListener(actListener);
		
		JPanel pnlButton = new JPanel(new FlowLayout());
		pnlButton.add(btOk);
		pnlButton.add(btStorno);
		
		
		
		this.add(pnlPolozky, "Center");
		this.add(pnlButton, "South");
		
		
	}

	protected void vyplnKontakt() {
		if (kontakt == null){
			kontakt = new Kontakt();
			kontakt.setAdresa(new Adresa());
		}
		
		kontakt.setJmeno(tfJmeno.getText());
		kontakt.setPrijmeni(tfPrijmeni.getText());
		kontakt.setTelefon(tfTelefon.getText());
		kontakt.getAdresa().setUlice(tfUlice.getText());
		kontakt.getAdresa().setCp(tfCp.getText());
		kontakt.getAdresa().setObec(tfObec.getText());
		kontakt.getAdresa().setPsc(tfPsc.getText());
		
	}
	
	public boolean spustAdd(){
		ok = false; //pro kliknuti na storno
		kontakt = null;
		
		tfJmeno.grabFocus();
		setVisible(true);
		return ok;
	}
	
	public boolean spustEdit(Kontakt knt){
		ok = false;
		kontakt = knt;
		
		naplnTextfieldy();
		tfJmeno.grabFocus();
		tfJmeno.selectAll(); //oznaci vse v textovem poli
		setVisible(true);
		return ok;
	}
	
	private void naplnTextfieldy() {
		if(kontakt != null){
			tfJmeno.setText(kontakt.getJmeno());
			tfPrijmeni.setText(kontakt.getPrijmeni());
			tfTelefon.setText(kontakt.getTelefon());
			tfUlice.setText(kontakt.getAdresa().getUlice());
			tfCp.setText(kontakt.getAdresa().getCp());
			tfObec.setText(kontakt.getAdresa().getObec());
			tfPsc.setText(kontakt.getAdresa().getPsc());
		}
	}

	public void vymazTextfieldy(){
		tfJmeno.setText("");
		tfPrijmeni.setText("");
		tfTelefon.setText("");
		tfUlice.setText("");
		tfCp.setText("");
		tfObec.setText("");
		tfPsc.setText("");
	}

	public boolean IsOk(){
		return ok;
	}
	
	public void setKontakt(Kontakt kontakt) {
		this.kontakt = kontakt;
	}
	
	public Kontakt getKontakt() {
		return kontakt;
	}

}

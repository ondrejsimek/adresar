package logika;


import java.util.ArrayList;
import java.util.List;

import model.Kontakt;

public class InMemoryAdresarServices implements AdresarServices {
	private static long generator_id = 0L;	//0L ud�v�, �e se jedn� o long
	private List<Kontakt> kontakty = new ArrayList<Kontakt>();
	
	@Override
	public void store(Kontakt kontakt) {
		if(kontakt.getId()==0){
			//pridani noveho kontaktu
			kontakt.setId(++generator_id);
			kontakty.add(kontakt);
		} else {
			Kontakt knt = getById(kontakt.getId());
			
			//Overeni, zda neni reference stejna s tim, co uz je v seznamu.
			if (knt!=kontakt){
				int pozice = kontakty.indexOf(knt);
				kontakty.set(pozice, kontakt);
			}
		}
	}

	@Override
	public Kontakt getById(long idKontaktu) {
		for (Kontakt knt:kontakty){
			if(knt.getId()==idKontaktu){
				return knt;
			}
		}
		return null;
	}

	@Override
	public int getCount() {
		return kontakty.size();
	}

	@Override
	public void clear() {
		kontakty.clear();
	}

	@Override
	public void remove(long idKontaktu) {
		Kontakt knt = getById(idKontaktu);
		if(knt !=null){
			kontakty.remove(knt);
		}
	}
	
	@Override
	public List<Kontakt> getAll(){
		return kontakty;
	}
}



package logika;

import java.util.List;
import model.Kontakt;

public interface AdresarServices {
	/**
	 * Aktualizujenebo uklada kontakt podle toho zda je id rovno nule nebo neni. Pokud je id rovno nula pak by mel byt kontakt pridan (vcetne vygenerovani nenuloveho a jedinecneho id), jinak je aktualizovan.
	 * @param kontakt
	 */
	public void store(Kontakt kontakt);
	
	
	/**
	 * Vraci kontakt se zadanym id. Pokud neni takovy kontakt nalezen, vraci null
	 * @param idKontaktu
	 * @return
	 */
	public Kontakt getById(long idKontaktu);
	
	
	/**
	 * Vrac� po�et evidovan�ch kontakt�
	 * @return
	 */
	public int getCount();
	
	
	/**
	 * Vycisteni evidence, odstrani vsechny kontakty
	 */
	public void clear();
	
	
	/**
	 * Odstrani kontakt se zadanym id z evidence
	 * @param idKontaktu
	 */
	public void remove(long idKontaktu);
	
	
	/**
	 * Vraceni seznamu vsech evidovanych kontaktu
	 * @return
	 */
	public List<Kontakt> getAll();
}



